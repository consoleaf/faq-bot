FROM alpine as production
WORKDIR /bot
RUN apk update && apk add --no-cache tree python3 && \
    python3 -m ensurepip && \
    rm -r /usr/lib/python*/ensurepip && \
    pip3 install --upgrade pip setuptools && \
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
    rm -r /root/.cache
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev bind-tools
COPY tgBot/bot/requirements.txt ./
RUN pip install -r ./requirements.txt
COPY tgBot ./tgBot
COPY shared ./shared
ENV PYTHONPATH "${PYTHONPATH}:/bot"
CMD python tgBot/bot/bot.py