import Vue from 'vue';
import VueRouter from 'vue-router';
import Ping from '../views/Ping.vue';
import NotFound from '../views/NotFound.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/settings',
    name: 'Settings',
    component: () => import(/* webpackChunkName: Settings */ "../views/Settings"),
  },
  {
    path: '/chat',
    name: 'Chat',
    component: () => import(/* webpackChunkName: Chat */ "../views/Chat"),
  },
  {
    path: '/',
    name: 'Index',
    component: () => import(/* webpackChunkName: Index */ "../views/Index"),
  }
];

// Add 404
routes.push({
  path: "*",
  name: "NotFound",
  component: NotFound,
});

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
