from pony.orm import *
import os

db = Database()

# TODO: database models


db.bind(provider='postgres',
        host=os.getenv("POSTGRES_HOST"),
        user=os.getenv("POSTGRES_USER"),
        password=os.getenv("POSTGRES_PASSWORD"),
        database=os.getenv("POSTGRES_DB"))
db.generate_mapping(create_tables=True)
