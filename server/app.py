from flask import Flask, jsonify
from flask_cors import CORS
from shared.database import orm as db

# configuration
DEBUG = True

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# Enable CORS
CORS(app, resources={r"/*": {'origins': '*'}})

# TODO: API endpoints

if __name__ == "__main__":
    app.run()
