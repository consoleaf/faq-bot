import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";
import vuetify from "../plugins/vuetify";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    chats: [], // TODO specification
    messages: {}, // { user_id: [messages] }
    currentChatID: null, // user_id
    darkTheme: false,
  },
  mutations: {
    SET_CHATS(state, chatsList) {
      state.chats = chatsList;
    },
    SET_MESSAGES_FOR_USER(state, payload) {
      let pl = {};
      pl[payload.uid] = payload.messagesList;
      state.messages = {...state.messages, ...pl};
    },
    SET_ACTIVE_CHAT(state, newChatID) {
      state.currentChatID = newChatID;
    },
    TOGGLE_DARK_THEME(state) {
      state.darkTheme = !state.darkTheme;
      vuetify.framework.theme.dark = state.darkTheme;
    }
  },
  actions: {
    loadChats(context,) {
      // Dummy method
      setTimeout(() => {
        let chats = [
          {
            uid: 1,
            name: "Steve",
            avatar: "https://cdn.vuetifyjs.com/images/lists/1.jpg",
            unreadCnt: 0,
            lastMsg: "Last message text lorem ipsum dolor sit amet lalalala"
          },
          {
            uid: 2,
            name: "Adam",
            avatar: "https://cdn.vuetifyjs.com/images/lists/2.jpg",
            unreadCnt: 5,
            lastMsg: "Last message text?"
          }
        ];
        context.commit("SET_CHATS", chats);
      }, 500);
    },
    loadMessages(context, payload) {
      let chat_id = payload.uid;
      // Dummy method
      setTimeout(() => {
        let size = Math.round(Math.random() * 5 + 15);
        let messages = [];
        for (let message_id = 0; message_id < size; message_id++) {
          messages.push({
            message_id,
            text: "Lorem ipsum dolor sit amet http://ya.ru",
            author: (Math.random() > 0.5 ? "me" : "them")
          });
        }
        context.commit("SET_MESSAGES_FOR_USER", {'uid': chat_id, 'messagesList': messages});
        // this.messages[chat_id] = messages;
        // Had to change, Vue can't detect
      }, 500);
    },
    changeChat(context, payload) {
      // Load the messages
      context.commit("SET_ACTIVE_CHAT", payload)
      context.dispatch('loadMessages', {'uid': payload}).then(r => console.log("Messages loaded!"));
    }
  },
  modules: {},
  plugins: [createPersistedState()],
})
