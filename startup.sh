#!/bin/bash

sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf
# shellcheck disable=SC2164
cd server
GUNICORN_CMD_ARGS="--access-logfile gunicorn.access.log --error-logfile gunicorn.error.log" gunicorn -b 0.0.0.0:5000 app:app --daemon
nginx
touch gunicorn.access.log
touch gunicorn.error.log
tail -f gunicorn.access.log -f gunicorn.error.log -f /var/log/nginx/access.log -f /var/log/nginx/error.log
