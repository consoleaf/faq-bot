import telebot
from telebot import apihelper
from shared.database import orm as db
import os
import logging

logging.basicConfig(level=logging.DEBUG)

# Proxy
if os.getenv("PROXY"):
    print("Setting proxy", os.getenv("PROXY"))
    apihelper.proxy = {"https": "socks5h://" + os.getenv("PROXY")}

# instantiate the bot
bot = telebot.TeleBot(os.getenv("TG_TOKEN"), threaded=False)

# TODO: Message handlers


if __name__ == "__main__":
    bot.polling(none_stop=False)
